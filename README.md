Node.js with MongoDB Sample [![Build Status](https://apibeta.shippable.com/projects/5373eb2df39baf5a00c05c89/badge/master)](https://beta.shippable.com/projects/5373eb2df39baf5a00c05c89)
=================

Implements a simple web application using MongoDB, with tests.

Uses Grunt to run tests against an Express server, then generates reports with Xunit and Istanbul.

This sample is built for Shippable, a docker based continuous integration and deployment platform.

[![Run Status]( https://apibeta.shippable.com/projects/56e10ca4c77dae78a8f4711c/badge?branch=master)]

